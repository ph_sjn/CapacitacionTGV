﻿using System;
using System.Collections.Generic;

namespace Data.Model
{
    public partial class Clientes
    {
        public short Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
    }
}
