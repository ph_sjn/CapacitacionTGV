/// <reference path="../../wwwroot/js/typings/natal/nf.d.ts" />
'use strict';
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Web;
(function (Web) {
    var HomeApiHelper;
    (function (HomeApiHelper) {
        var Home = /** @class */ (function (_super) {
            __extends(Home, _super);
            function Home() {
                return _super.call(this, '/Home') || this;
            }
            Home.prototype.GetDatosHome = function () {
                return this.get(this.controller + '/GetDatosHome', {});
            };
            return Home;
        }(ApiHelper.ApiHelperBase));
        HomeApiHelper.Home = Home;
    })(HomeApiHelper = Web.HomeApiHelper || (Web.HomeApiHelper = {}));
})(Web || (Web = {}));
