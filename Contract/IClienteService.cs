﻿using Data.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Contract
{
    public interface IClienteService
    {
        IEnumerable<Clientes> Search();
    }
}
