/// <reference path="../wwwroot/js/typings/jquery/jquery.d.ts" />
/// <reference path="../wwwroot/js/typings/lodash/lodash.d.ts" />
/// <reference path="../wwwroot/js/typings/natal/nf.d.ts" />
//$(".ajaxify").on("click", (event) => this.ajaxify(event));
//function ajaxify(evt) {
//    var a = $(evt.target);
//    var target = $('#' + a.attr('target'));
//    target.load(a.attr('href'));
//    return false;
//}
var Common;
(function (Common) {
    function isArray(obj) {
        return Object.prototype.toString.call(obj) === '[object Array]';
    }
    Common.isArray = isArray;
    var Controller = /** @class */ (function () {
        function Controller() {
            this.UID = new Date().getTime();
        }
        Controller.prototype.getName = function () {
            var funcNameRegex = /function (.{1,})\(/;
            var results = (funcNameRegex).exec(this.constructor.toString());
            return (results && results.length > 1) ? results[1] : "";
        };
        Controller.registerController = function (c) {
            if (!Common.Controller.current) {
                // para un unico controller registrarlo
                Common.Controller.current = c;
            }
            else if (Object.prototype.toString.call(Common.Controller.current) === '[object Array]') {
                // si ya tengo un array de controllers lo agrego
                Common.Controller.current.push(c);
            }
            else {
                // si tengo un unico controller cambio por un array y lo agrego
                Common.Controller.current = [Common.Controller.current, c];
            }
        };
        Controller.initialize = function (controllerType, entityUrl, initialDataUrl) {
            var controller;
            if (initialDataUrl) {
                $.ajax(initialDataUrl, {
                    type: "POST",
                    success: function (data) {
                        controller = new controllerType(data);
                        controller.serviceUrl = entityUrl;
                        Common.Controller.registerController(controller);
                    }
                });
            }
            else {
                controller = new controllerType();
                controller.serviceUrl = entityUrl;
                Common.Controller.registerController(controller);
            }
        };
        //static initializeChild(controllerType: { new (any?); }, params: any, rootId: string) {
        //    var controller;
        //    controller = new controllerType(params);
        //    //var promise = ko.applyBindings(controller, document.getElementById(rootId));
        //    Common.Controller.registerController(controller);
        //    return promise;
        //}
        Controller.initializeChild = function (controller) {
            Common.Controller.registerController(controller);
            return controller;
        };
        //static initializeChildIfControllerNotExist(controllerType: { new (any?); }, params: any, rootId: string) {
        //    var controller;
        //    var promise;
        //    var controllerName: any = controllerType;
        //    controller = Common.Controller.getControllerByName(controllerName.name);
        //    if (controller == null) {
        //        controller = new controllerType(params);
        //        promise = ko.applyBindings(controller, document.getElementById(rootId));
        //        Common.Controller.registerController(controller);
        //    }
        //    else {
        //        promise = ko.applyBindings(controller, document.getElementById(rootId));
        //    }
        //    return promise;
        //}
        //static initializeChilds(controllerType: { new (any?); }, params: any, root: string) {
        //    var controller;
        //    controller = new controllerType(params);
        //    $.each($(root), (index: any, value: any) => {
        //        ko.applyBindings(controller, value);
        //    });
        //    Common.Controller.registerController(controller);
        //    return controller;
        //}
        Controller.getController = function (uid) {
            if ($.isArray(Common.Controller.current)) {
                var results = Common.Controller.current.filter(function (x) { return x.UID == uid; });
                if (results != undefined) {
                    return results[0];
                }
            }
            return null;
        };
        Controller.getControllerByName = function (name) {
            if ($.isArray(Common.Controller.current)) {
                var results = Common.Controller.current.filter(function (x) { return x.getName() == name; });
                if ((results != undefined) && (results.length > 0)) {
                    return results[0];
                }
            }
            else {
                if (Common.Controller.current.getName() == name) {
                    return Common.Controller.current;
                }
            }
            return null;
        };
        Controller.downloadFile = function (url) {
            window.open(url, '_blank');
            $('body').append('<a href=\'' + url + '\' target=\'_blank\' id=\'downloadLinkHasheable\'>');
            $("#downloadLinkHasheable").click();
            $('body').remove("#downloadLinkHasheable");
        };
        //#endregion        
        Controller.prototype.create = function () {
            this.submit('POST', this.serviceUrl);
        };
        Controller.prototype.update = function () {
            this.submit('PUT', this.serviceUrl);
        };
        Controller.prototype.submit = function (method, url) {
            if (this.hasErrors())
                return;
            var requestData = JSON.stringify(this.Model);
            $.ajax(url, {
                context: this,
                type: method,
                contentType: 'application/json; charset=utf-8',
                data: requestData,
                success: this.success,
                error: this.error
            });
        };
        Controller.prototype.get = function (url, params, successFunction) {
            if (params != undefined) {
                url += '?' + jQuery.param(params);
            }
            $.ajax({
                type: "GET",
                url: url,
                contentType: 'application/json; charset=utf-8',
                error: this.error,
                success: successFunction
            });
        };
        Controller.prototype.hasErrors = function () {
            return !NF.Validation.validate();
        };
        Controller.prototype.error = function (jqXHR, textStatus, errorThrow) {
            NF.Notification.error('Error', 'Error en el formulario: ' + jqXHR.responseJSON.ModelState, true, 'TopLeft');
        };
        Controller.prototype.success = function (responseData, textStatus, jqXHR) {
        };
        return Controller;
    }());
    Common.Controller = Controller;
    var Constants;
    (function (Constants) {
        Constants.ShortDate = "dd/MM/yy";
        Constants.RegularDate = "dd/MM/yyyy";
    })(Constants = Common.Constants || (Common.Constants = {}));
    var Notifications;
    (function (Notifications) {
        function ShowOKMessage(message, title) {
            NF.Notification.show({
                type: NF.Notification.Type.SUCCESS,
                title: title,
                content: message,
                position: NF.Notification.Positions.TOP_RIGHT,
                clickClose: true,
                autoHideDelay: 3000
            });
        }
        Notifications.ShowOKMessage = ShowOKMessage;
        function ShowERRORMessage(message, title) {
            NF.Notification.show({
                type: NF.Notification.Type.ERROR,
                title: title,
                content: message,
                position: NF.Notification.Positions.TOP_RIGHT,
                clickClose: true,
                autoHideDelay: 3000
            });
        }
        Notifications.ShowERRORMessage = ShowERRORMessage;
        function ShowINFOMessage(message, title) {
            NF.Notification.show({
                type: NF.Notification.Type.NOTICE,
                title: title,
                content: message,
                position: NF.Notification.Positions.TOP_RIGHT,
                clickClose: true,
                autoHideDelay: 3000
            });
        }
        Notifications.ShowINFOMessage = ShowINFOMessage;
    })(Notifications = Common.Notifications || (Common.Notifications = {}));
    var DateFunctions;
    (function (DateFunctions) {
        ///Toma una fecha con formato dd/mm/YY, retornando un objeto Date
        function ToDate(date) {
            var regexpString = "^([0]?[1-9]|[1|2][0-9]|[3][0|1])[./-]([0]?[1-9]|[1][0-2])[./-]([0-9]{4}|[0-9]{2})$";
            var regexp = new RegExp(regexpString);
            var result = regexp.exec(date);
            var parsedDate;
            if (result != null) {
                var parsedDate = result[2] + "/" + result[1] + "/" + result[3];
                return new Date(parsedDate);
            }
            return null;
        }
        DateFunctions.ToDate = ToDate;
        ///Toma una fecha con formato ISO YYYY/mm/ddThh:mm:ss, retornando un objeto Date
        function IsoToDate(date) {
            var regexpString = "^([0-9]{4}|[0-9]{2})[-]([0]?[1-9]|[1][0-2])[-]([0]?[1-9]|[1|2][0-9]|[3][0|1])[tT]([0]?[0-9]|[1|2][0-9])[:]([0]?[0-9]|[1-5][0-9])[:]([0]?[0-9]|[1-5][0-9])?$";
            var regexp = new RegExp(regexpString);
            var result = regexp.exec(date);
            var parsedDate;
            if (result != null) {
                return new Date(parseInt(result[1]), parseInt(result[2]) - 1, parseInt(result[3]), parseInt(result[4]), parseInt(result[5]), parseInt(result[6]));
            }
            return null;
        }
        DateFunctions.IsoToDate = IsoToDate;
        ///Toma una fecha con formato ISO YYYY/mm/ddThh:mm:ss, retornando un string dd/mm/YYYY
        function IsoToStringDate(date) {
            var regexpString = "^([0-9]{4}|[0-9]{2})[-]([0]?[1-9]|[1][0-2])[-]([0]?[1-9]|[1|2][0-9]|[3][0|1])[tT]([0]?[0-9]|[1|2][0-9])[:]([0]?[0-9]|[1-5][0-9])[:]([0]?[0-9]|[1-5][0-9])?$";
            var regexp = new RegExp(regexpString);
            var result = regexp.exec(date);
            var parsedDate;
            if (result != null) {
                return (result[3] + '/' + result[2] + '/' + result[1]);
            }
            return null;
        }
        DateFunctions.IsoToStringDate = IsoToStringDate;
        function Compare(a, b) {
            if (a > b)
                return 1;
            if (a < b)
                return -1;
            return 0;
        }
        DateFunctions.Compare = Compare;
        function GetDaysInMonth(date) {
            var month = date.getMonth();
            var isLeap = isLeap = new Date(date.getFullYear(), 1, 29).getMonth() == 1;
            return [31, (isLeap ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
        }
        DateFunctions.GetDaysInMonth = GetDaysInMonth;
        function GetUTCTime(date, showSeconds) {
            if (showSeconds === void 0) { showSeconds = false; }
            if (date == undefined || date == null) {
                console.error("Se ha intentado obtener la hora desde un objeto sin valor. Date = " + date);
                return "";
            }
            var hours = date.getUTCHours().toPrecision(2);
            var minutes = date.getMinutes().toPrecision(2);
            var resul = hours + ":" + minutes;
            if (showSeconds) {
                resul += ":" + date.getUTCSeconds().toPrecision(2);
            }
            return resul;
        }
        DateFunctions.GetUTCTime = GetUTCTime;
        function GetDateToday() {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1;
            var yyyy = today.getFullYear();
            var todayFormat = dd + '/' + mm + '/' + yyyy;
            return todayFormat;
        }
        DateFunctions.GetDateToday = GetDateToday;
        function ToMMDDYYYYString(date) {
            if (date != null) {
                var dd = date.getDate();
                var mm = date.getMonth() + 1;
                var yyyy = date.getFullYear();
                return mm + "/" + dd + "/" + yyyy;
            }
            else {
                return null;
            }
        }
        DateFunctions.ToMMDDYYYYString = ToMMDDYYYYString;
        function GetIntFromPeriod(period) {
            if (period == null) {
                return null;
            }
            var temp = period.split("/");
            return parseInt(temp[1]) * 100 + parseInt(temp[0]);
        }
        DateFunctions.GetIntFromPeriod = GetIntFromPeriod;
        function GetPeriodFromInt(period) {
            if (period == null) {
                return null;
            }
            var yyyy = ("0000" + Math.floor(period / 100).toString()).slice(-4);
            var mm = ("00" + (period % 100).toString()).slice(-2);
            return yyyy + "/" + mm;
        }
        DateFunctions.GetPeriodFromInt = GetPeriodFromInt;
    })(DateFunctions = Common.DateFunctions || (Common.DateFunctions = {}));
    var Templates = /** @class */ (function () {
        function Templates() {
        }
        Templates.LoadTemplate = function (name) {
            $('body').append('<div id="templates"></div>');
            $('#templates').load("../Features/Shared/Templates/" + name + "Template.html");
        };
        Templates.LoadTemplates = function (names) {
            $('body').append('<div id="templates"></div>');
            for (var i = 0; i < names.length; i++) {
                var div = $('<div id="' + names[i] + '"></div>');
                $('#templates').append(div);
                div.load("../Features/Shared/Templates/" + names[i] + "Template.html");
            }
        };
        return Templates;
    }());
    Common.Templates = Templates;
    var Autocomplete;
    (function (Autocomplete) {
        var Item = /** @class */ (function () {
            function Item(id, text) {
                this.id = id;
                this.text = text;
            }
            return Item;
        }());
        Autocomplete.Item = Item;
        var Result = /** @class */ (function () {
            function Result(results) {
                this.results = results;
            }
            return Result;
        }());
        Autocomplete.Result = Result;
    })(Autocomplete = Common.Autocomplete || (Common.Autocomplete = {}));
    var Format;
    (function (Format) {
        Format.Money = function (value) {
            var lang = numeral.language();
            numeral.language('es', {
                delimiters: {
                    thousands: '.',
                    decimal: ','
                },
                abbreviations: {
                    thousand: 'k',
                    million: 'mm',
                    billion: 'b',
                    trillion: 't'
                },
                ordinal: function (number) {
                    var b = number % 10;
                    return (b === 1 || b === 3) ? 'er' :
                        (b === 2) ? 'do' :
                            (b === 7 || b === 0) ? 'mo' :
                                (b === 8) ? 'vo' :
                                    (b === 9) ? 'no' : 'to';
                },
                currency: {
                    symbol: '$'
                }
            });
            numeral.language('es');
            var format = numeral(value).format('$0,0.00');
            numeral.language(lang);
            return format;
        };
        Format.Float = function (value) {
            if ($.isNumeric(value)) {
                var lang = numeral.language();
                numeral.language('es', {
                    delimiters: {
                        thousands: '.',
                        decimal: ','
                    },
                    abbreviations: {
                        thousand: 'k',
                        million: 'mm',
                        billion: 'b',
                        trillion: 't'
                    },
                    ordinal: function (number) {
                        var b = number % 10;
                        return (b === 1 || b === 3) ? 'er' :
                            (b === 2) ? 'do' :
                                (b === 7 || b === 0) ? 'mo' :
                                    (b === 8) ? 'vo' :
                                        (b === 9) ? 'no' : 'to';
                    },
                    currency: {
                        symbol: '$'
                    }
                });
                numeral.language('es');
                var format = numeral(value).format('0,0.[00]');
                numeral.language(lang);
                return format;
            }
            else
                return null;
        };
        Format.FloatFixed = function (value) {
            var lang = numeral.language();
            numeral.language('es', {
                delimiters: {
                    thousands: '.',
                    decimal: ','
                },
                abbreviations: {
                    thousand: 'k',
                    million: 'mm',
                    billion: 'b',
                    trillion: 't'
                },
                ordinal: function (number) {
                    var b = number % 10;
                    return (b === 1 || b === 3) ? 'er' :
                        (b === 2) ? 'do' :
                            (b === 7 || b === 0) ? 'mo' :
                                (b === 8) ? 'vo' :
                                    (b === 9) ? 'no' : 'to';
                },
                currency: {
                    symbol: '$'
                }
            });
            numeral.language('es');
            var format = numeral(value).format('0,0.00');
            numeral.language(lang);
            return format;
        };
        Format.Fixed = function (value) {
            return value > 9 ? value.toString() : "0" + value;
        };
        Format.Phone = function (value) {
            var areaCode = value.substring(0, value.indexOf("-"));
            var number = value.substring(value.indexOf("-") + 1, value.length);
            var numberPart2 = number.substring(number.length - 4, number.length);
            var numberPart1 = number.substring(0, number.length - 4);
            return "(" + areaCode + ") " + numberPart1 + " " + numberPart2;
        };
        Format.CUIL = function (value) {
            var regexp = /^(\d{2})(\d*)(\d{1})(.*)$/g;
            var regexp80 = /^(80)(.*)$/g;
            var resultwith80 = regexp80.exec(value);
            if (resultwith80 && resultwith80.length == 3)
                value = resultwith80[2];
            var result = regexp.exec(value);
            if (result.length == 5 && result[4] == "")
                return (result[1] + "-" + result[2] + "-" + result[3]);
            if (result.length == 5)
                return (result[1] + "-" + result[2] + "-" + result[3] + " - " + result[4]);
            else
                return value;
        };
    })(Format = Common.Format || (Common.Format = {}));
    var DataTables;
    (function (DataTables) {
        var Format;
        (function (Format) {
            var yesno = /** @class */ (function () {
                function yesno() {
                    this.render = function (data, type, row) {
                        var bData = $.parseJSON(data);
                        if (typeof bData == "boolean")
                            return bData ? 'Sí' : 'No';
                        else
                            return data;
                    };
                }
                return yesno;
            }());
            var money = /** @class */ (function () {
                function money() {
                    this.render = function (data, type, row) {
                        return Common.Format.Money(data);
                    };
                }
                return money;
            }());
            var dashOnNull = /** @class */ (function () {
                function dashOnNull() {
                    this.render = function (data, type, row) {
                        if (data == null || data == '' || data != null && data.toString().trim() == '')
                            return '-';
                        else
                            return data;
                    };
                }
                return dashOnNull;
            }());
            var dashOnZero = /** @class */ (function () {
                function dashOnZero() {
                    this.render = function (data, type, row) {
                        if (data == 0)
                            return '-';
                        else
                            return data;
                    };
                }
                return dashOnZero;
            }());
            var float = /** @class */ (function () {
                function float() {
                    this.render = function (data, type, row) {
                        return Common.Format.FloatFixed(data);
                    };
                }
                return float;
            }());
            var floatFixed = /** @class */ (function () {
                function floatFixed() {
                    this.render = function (data, type, row) {
                        return Common.Format.FloatFixed(data);
                    };
                }
                return floatFixed;
            }());
            var small = /** @class */ (function () {
                function small() {
                    this.render = function (data, type, row) {
                        return '<small>' + data + '</small>';
                    };
                }
                return small;
            }());
            Format.YesNo = new yesno();
            Format.Money = new money();
            Format.Float = new float();
            Format.FloatFixed = new floatFixed();
            Format.DashOnNull = new dashOnNull();
            Format.DashOnZero = new dashOnZero();
            Format.Small = new small();
            Format.Create = function (targets, format) {
                return {
                    targets: targets,
                    render: format.render
                };
            };
        })(Format = DataTables.Format || (DataTables.Format = {}));
    })(DataTables = Common.DataTables || (Common.DataTables = {}));
    var Utilities;
    (function (Utilities) {
        Utilities.Url = function (relativeUrl, host) {
            if (host) {
                return host + '/' + relativeUrl;
            }
            return document.location.protocol + '//' + document.location.host + '/' + relativeUrl;
        };
        Utilities.parseMoneyToDecimal = function (money) {
            if (money != null) {
                if ($.isNumeric(money)) {
                    return money;
                }
                ;
                return parseFloat(money.replace(",", "&").replace(".", ",").replace("&", "."));
            }
            else {
                return null;
            }
        };
        Utilities.SelectRow = function (table, index) {
            //TODO: Agregar DataTable para que lo entienda un objeto JQuery
            var $currentRow = $($(table)["DataTable"]().rows().nodes().to$()[index]);
            /*$currentRow.addClass('selected');
            $currentRow.find('td>i').addClass('fa-check-square');*/
            $currentRow.click();
        };
        Utilities.stringToDecimal = function (money) {
            if (Object.prototype.toString.call(money) == '[object String]') {
                money = money.replace(",", ".");
            }
            if ($.isNumeric(money)) {
                return parseFloat(money);
            }
            return null;
        };
    })(Utilities = Common.Utilities || (Common.Utilities = {}));
    var Cors;
    (function (Cors) {
        function PerformGetHtml(sourceUrl, token, onSuccess) {
            $.ajax({
                type: 'GET',
                url: sourceUrl,
                context: this,
                headers: {
                    'Authorization': 'Bearer ' + token
                },
                success: onSuccess
            });
        }
        Cors.PerformGetHtml = PerformGetHtml;
        // Realiza el llamado a un action de una sourceUrl dada. 
        // resourcesUrls es un array con las urls de los recursos necesarios para que funcione el html.
        // token es el token necesario para conectarse a tráves de Auth0
        // onSuccess es la función que se retorna en success luego de obtener el html pedido.
        function GetHtml(sourceUrl, resourcesUrls, token, onSuccess) {
            if (resourcesUrls == undefined || !$.isArray(resourcesUrls) || resourcesUrls.length == 0) {
                Cors.PerformGetHtml(sourceUrl, token, onSuccess);
            }
            else {
                var totalJsReady = 0;
                var totalJsToLoad = 2;
                var callbackIn = function () {
                    totalJsReady++;
                    if (totalJsReady == totalJsToLoad) {
                        Cors.PerformGetHtml(sourceUrl, token, onSuccess);
                    }
                };
                for (var i = 0; i < resourcesUrls.length; i++) {
                    $.ajax({
                        type: 'GET',
                        url: resourcesUrls[i],
                        context: this,
                        dataType: "script",
                        headers: {
                            'Authorization': 'Bearer ' + $("#testCors").val()
                        },
                        success: callbackIn
                    });
                }
            }
        }
        Cors.GetHtml = GetHtml;
    })(Cors = Common.Cors || (Common.Cors = {}));
    var CourtProceedingDefinition;
    (function (CourtProceedingDefinition) {
        var CourtProceedingDefinitionType;
        (function (CourtProceedingDefinitionType) {
            CourtProceedingDefinitionType[CourtProceedingDefinitionType["Trial"] = 1] = "Trial";
            CourtProceedingDefinitionType[CourtProceedingDefinitionType["Agree"] = 2] = "Agree";
            CourtProceedingDefinitionType[CourtProceedingDefinitionType["Mediation"] = 3] = "Mediation";
        })(CourtProceedingDefinitionType = CourtProceedingDefinition.CourtProceedingDefinitionType || (CourtProceedingDefinition.CourtProceedingDefinitionType = {}));
        function toString(mode) {
            if (mode == CourtProceedingDefinitionType.Trial) {
                return "Juicio";
            }
            else if (mode == CourtProceedingDefinitionType.Mediation) {
                return "Mediación";
            }
            else if (mode == CourtProceedingDefinitionType.Agree) {
                return "Acuerdo/Gestión Extrajudicial";
            }
        }
        CourtProceedingDefinition.toString = toString;
    })(CourtProceedingDefinition = Common.CourtProceedingDefinition || (Common.CourtProceedingDefinition = {}));
})(Common || (Common = {}));
