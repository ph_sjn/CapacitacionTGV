﻿using Contract;
using Microsoft.AspNetCore.Mvc;
using System.Collections;
using System.Threading.Tasks;
using Web.Models;

namespace Web
{

    [Produces("application/json")]
    [Route("api/Home")]
    public class HomeController : Controller
    {
        private readonly IClienteService _clienteService;

        public HomeController(IClienteService clienteService)
        {
            _clienteService = clienteService;
        }

        [HttpGet]
        [Route("GetDatosHome")]
        public ActionResult GetDatosHome()
        {
            IEnumerable cliente = _clienteService.Search();

            return Ok(cliente);
        }
    }
}