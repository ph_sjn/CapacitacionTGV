/// <reference path="../wwwroot/js/typings/jquery/jquery.d.ts" />
/// <reference path="../wwwroot/js/typings/lodash/lodash.d.ts" />
/// <reference path="../wwwroot/js/typings/natal/nf.d.ts" />
//$(".ajaxify").on("click", (event) => this.ajaxify(event));

//function ajaxify(evt) {
//    var a = $(evt.target);
//    var target = $('#' + a.attr('target'));
//    target.load(a.attr('href'));
//    return false;
//}


module Common {
    export function isArray(obj: any): boolean {
        return Object.prototype.toString.call(obj) === '[object Array]';
    }

    export class Controller<TModel> {
        // #region constructor and factory

        static current;

        getName() {
            var funcNameRegex = /function (.{1,})\(/;
            var results = (funcNameRegex).exec((<any>this).constructor.toString());
            return (results && results.length > 1) ? results[1] : "";
        }

        static registerController(c: any) {

            if (!Common.Controller.current) {
                // para un unico controller registrarlo
                Common.Controller.current = c;
            } else if (Object.prototype.toString.call(Common.Controller.current) === '[object Array]') {
                // si ya tengo un array de controllers lo agrego
                Common.Controller.current.push(c);
            } else {
                // si tengo un unico controller cambio por un array y lo agrego
                Common.Controller.current = [Common.Controller.current, c];
            }
        }

        static initialize(controllerType: { new(any?); }, entityUrl: string, initialDataUrl?: string) {

            var controller;

            if (initialDataUrl) {
                $.ajax(initialDataUrl, {
                    type: "POST",
                    success: (data) => {
                        controller = new controllerType(data);

                        controller.serviceUrl = entityUrl;

                        Common.Controller.registerController(controller);
                    }
                });

            } else {
                controller = new controllerType();

                controller.serviceUrl = entityUrl;

                Common.Controller.registerController(controller);
            }
        }

        //static initializeChild(controllerType: { new (any?); }, params: any, rootId: string) {

        //    var controller;

        //    controller = new controllerType(params);

        //    //var promise = ko.applyBindings(controller, document.getElementById(rootId));
        //    Common.Controller.registerController(controller);

        //    return promise;
        //}

        static initializeChild(controller: any) {

            Common.Controller.registerController(controller);
            return controller;
        }


        //static initializeChildIfControllerNotExist(controllerType: { new (any?); }, params: any, rootId: string) {

        //    var controller;
        //    var promise;
        //    var controllerName: any = controllerType;

        //    controller = Common.Controller.getControllerByName(controllerName.name);
        //    if (controller == null) {
        //        controller = new controllerType(params);

        //        promise = ko.applyBindings(controller, document.getElementById(rootId));
        //        Common.Controller.registerController(controller);
        //    }
        //    else {
        //        promise = ko.applyBindings(controller, document.getElementById(rootId));
        //    }

        //    return promise;
        //}

        //static initializeChilds(controllerType: { new (any?); }, params: any, root: string) {

        //    var controller;

        //    controller = new controllerType(params);

        //    $.each($(root), (index: any, value: any) => {
        //        ko.applyBindings(controller, value);
        //    });

        //    Common.Controller.registerController(controller);
        //    return controller;
        //}



        static getController(uid: number) {
            if ($.isArray(Common.Controller.current)) {
                var results = Common.Controller.current.filter(function (x) { return x.UID == uid });
                if (results != undefined) {
                    return results[0];
                }
            }
            return null;
        }

        static getControllerByName(name: string) {
            if ($.isArray(Common.Controller.current)) {
                var results = Common.Controller.current.filter(function (x) { return x.getName() == name });

                if ((results != undefined) && (results.length > 0)) {
                    return results[0];
                }
            }
            else {
                if (Common.Controller.current.getName() == name) {
                    return Common.Controller.current;
                }
            }
            return null;
        }

        static downloadFile(url: string) {
            window.open(url, '_blank');
            $('body').append('<a href=\'' + url + '\' target=\'_blank\' id=\'downloadLinkHasheable\'>');
            $("#downloadLinkHasheable").click();
            $('body').remove("#downloadLinkHasheable");
        }

        public serviceUrl: string

        constructor() {
            this.UID = new Date().getTime();
        }

        //#endregion        

        //#region Properties

        Model: TModel;

        UID: number;

        //#endregion        

        create(): void {
            this.submit('POST', this.serviceUrl);
        }

        update(): void {
            this.submit('PUT', this.serviceUrl);
        }

        submit(method: string, url: string) {

            if (this.hasErrors()) return;

            var requestData = JSON.stringify(this.Model);

            $.ajax(url, {
                context: this,
                type: method,
                contentType: 'application/json; charset=utf-8',
                data: requestData,
                success: this.success,
                error: this.error
            });
        }


        get(url: string, params: any, successFunction: (data: any) => void) {

            if (params != undefined) {
                url += '?' + jQuery.param(params);
            }

            $.ajax({
                type: "GET",
                url: url,
                contentType: 'application/json; charset=utf-8',
                error: this.error,
                success: successFunction
            });
        }


        hasErrors(): boolean {
            return !NF.Validation.validate();
        }

        error(jqXHR: JQueryXHR, textStatus: string, errorThrow: string) {
            NF.Notification.error('Error',
                'Error en el formulario: ' + jqXHR.responseJSON.ModelState,
                true, 'TopLeft');
        }

        success(responseData: any, textStatus: string, jqXHR: JQueryXHR) {
        }

    }

    export module Constants {
        export var ShortDate = "dd/MM/yy";
        export var RegularDate = "dd/MM/yyyy";
    }

    export module Notifications {
        export function ShowOKMessage(message: string, title: string) {
            NF.Notification.show({
                type: NF.Notification.Type.SUCCESS,
                title: title,
                content: message,
                position: NF.Notification.Positions.TOP_RIGHT,
                clickClose: true,
                autoHideDelay: 3000
            });
        }
        export function ShowERRORMessage(message: string, title: string) {
            NF.Notification.show({
                type: NF.Notification.Type.ERROR,
                title: title,
                content: message,
                position: NF.Notification.Positions.TOP_RIGHT,
                clickClose: true,
                autoHideDelay: 3000
            });
        }
        export function ShowINFOMessage(message: string, title: string) {
            NF.Notification.show({
                type: NF.Notification.Type.NOTICE,
                title: title,
                content: message,
                position: NF.Notification.Positions.TOP_RIGHT,
                clickClose: true,
                autoHideDelay: 3000
            });
        }
    }
    export module DateFunctions {

        ///Toma una fecha con formato dd/mm/YY, retornando un objeto Date
        export function ToDate(date: string): Date {

            var regexpString = "^([0]?[1-9]|[1|2][0-9]|[3][0|1])[./-]([0]?[1-9]|[1][0-2])[./-]([0-9]{4}|[0-9]{2})$";
            var regexp = new RegExp(regexpString);
            var result = regexp.exec(date);
            var parsedDate: string;
            if (result != null) {
                var parsedDate = result[2] + "/" + result[1] + "/" + result[3];
                return new Date(parsedDate);
            }

            return null;
        }

        ///Toma una fecha con formato ISO YYYY/mm/ddThh:mm:ss, retornando un objeto Date
        export function IsoToDate(date: string): Date {
            var regexpString = "^([0-9]{4}|[0-9]{2})[-]([0]?[1-9]|[1][0-2])[-]([0]?[1-9]|[1|2][0-9]|[3][0|1])[tT]([0]?[0-9]|[1|2][0-9])[:]([0]?[0-9]|[1-5][0-9])[:]([0]?[0-9]|[1-5][0-9])?$";
            var regexp = new RegExp(regexpString);
            var result = regexp.exec(date);
            var parsedDate: string;
            if (result != null) {
                return new Date(parseInt(result[1]), parseInt(result[2]) - 1, parseInt(result[3]), parseInt(result[4]), parseInt(result[5]), parseInt(result[6]));
            }

            return null;
        }

        ///Toma una fecha con formato ISO YYYY/mm/ddThh:mm:ss, retornando un string dd/mm/YYYY
        export function IsoToStringDate(date: string): string {
            var regexpString = "^([0-9]{4}|[0-9]{2})[-]([0]?[1-9]|[1][0-2])[-]([0]?[1-9]|[1|2][0-9]|[3][0|1])[tT]([0]?[0-9]|[1|2][0-9])[:]([0]?[0-9]|[1-5][0-9])[:]([0]?[0-9]|[1-5][0-9])?$";
            var regexp = new RegExp(regexpString);
            var result = regexp.exec(date);
            var parsedDate: string;
            if (result != null) {
                return (result[3] + '/' + result[2] + '/' + result[1]);
            }

            return null;
        }

        export function Compare(a: Date, b: Date): number {
            if (a > b) return 1;
            if (a < b) return -1;
            return 0;
        }

        export function GetDaysInMonth(date: Date): number {
            var month = date.getMonth();
            var isLeap = isLeap = new Date(date.getFullYear(), 1, 29).getMonth() == 1;
            return [31, (isLeap ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
        }

        export function GetUTCTime(date: Date, showSeconds = false): string {
            if (date == undefined || date == null) {
                console.error("Se ha intentado obtener la hora desde un objeto sin valor. Date = " + date);
                return "";
            }
            var hours = date.getUTCHours().toPrecision(2);
            var minutes = date.getMinutes().toPrecision(2);
            var resul = hours + ":" + minutes;
            if (showSeconds) {
                resul += ":" + date.getUTCSeconds().toPrecision(2);
            }

            return resul;
        }

        export function GetDateToday(): string {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1;

            var yyyy = today.getFullYear();

            var todayFormat = dd + '/' + mm + '/' + yyyy;

            return todayFormat;
        }

        export function ToMMDDYYYYString(date: Date): string {
            if (date != null) {
                var dd = date.getDate();
                var mm = date.getMonth() + 1;
                var yyyy = date.getFullYear();

                return mm + "/" + dd + "/" + yyyy;
            } else {
                return null;
            }
        }

        export function GetIntFromPeriod(period: any): number {
            if (period == null) {
                return null;
            }

            var temp = period.split("/");
            return parseInt(temp[1]) * 100 + parseInt(temp[0]);
        }

        export function GetPeriodFromInt(period: number): any {
            if (period == null) {
                return null;
            }

            var yyyy = ("0000" + Math.floor(period / 100).toString()).slice(-4);
            var mm = ("00" + (period % 100).toString()).slice(-2);
            return yyyy + "/" + mm;
        }
    }

    export class Templates {

        static LoadTemplate(name: string) {
            $('body').append('<div id="templates"></div>');
            $('#templates').load("../Features/Shared/Templates/" + name + "Template.html");
        }

        static LoadTemplates(names: Array<string>) {
            $('body').append('<div id="templates"></div>');
            for (var i = 0; i < names.length; i++) {
                var div = $('<div id="' + names[i] + '"></div>');
                $('#templates').append(div);
                div.load("../Features/Shared/Templates/" + names[i] + "Template.html");
            }
        }

    }

    export module Autocomplete {

        export interface Query {
            element: any;
            term: string;
            page: number;
            callback: (data: Result) => any;
        }

        export class Item {
            id: any;
            text: string;
            disabled: boolean;
            locked: boolean;

            constructor(id: any, text: string) {
                this.id = id;
                this.text = text;
            }

        }

        export class Result {
            more: boolean;
            results: Array<any>;

            constructor(results: Array<Item>) {
                this.results = results;
            }

        }

    }

    export module Format {
        export var Money = (value: any): string => {
            var lang = numeral.language();
            numeral.language('es', {
                delimiters: {
                    thousands: '.',
                    decimal: ','
                },
                abbreviations: {
                    thousand: 'k',
                    million: 'mm',
                    billion: 'b',
                    trillion: 't'
                },
                ordinal: number => {
                    var b = number % 10;
                    return (b === 1 || b === 3) ? 'er' :
                        (b === 2) ? 'do' :
                            (b === 7 || b === 0) ? 'mo' :
                                (b === 8) ? 'vo' :
                                    (b === 9) ? 'no' : 'to';
                },
                currency: {
                    symbol: '$'
                }
            });
            numeral.language('es');
            var format = numeral(value).format('$0,0.00');
            numeral.language(lang);
            return format;
        };

        export var Float = (value: any): string => {
            if ($.isNumeric(value)) {
                var lang = numeral.language();
                numeral.language('es', {
                    delimiters: {
                        thousands: '.',
                        decimal: ','
                    },
                    abbreviations: {
                        thousand: 'k',
                        million: 'mm',
                        billion: 'b',
                        trillion: 't'
                    },
                    ordinal: number => {
                        var b = number % 10;
                        return (b === 1 || b === 3) ? 'er' :
                            (b === 2) ? 'do' :
                                (b === 7 || b === 0) ? 'mo' :
                                    (b === 8) ? 'vo' :
                                        (b === 9) ? 'no' : 'to';
                    },
                    currency: {
                        symbol: '$'
                    }
                });
                numeral.language('es');
                var format = numeral(value).format('0,0.[00]');
                numeral.language(lang);
                return format;
            } else return null;
        };

        export var FloatFixed = (value: any): string => {
            var lang = numeral.language();
            numeral.language('es', {
                delimiters: {
                    thousands: '.',
                    decimal: ','
                },
                abbreviations: {
                    thousand: 'k',
                    million: 'mm',
                    billion: 'b',
                    trillion: 't'
                },
                ordinal: number => {
                    var b = number % 10;
                    return (b === 1 || b === 3) ? 'er' :
                        (b === 2) ? 'do' :
                            (b === 7 || b === 0) ? 'mo' :
                                (b === 8) ? 'vo' :
                                    (b === 9) ? 'no' : 'to';
                },
                currency: {
                    symbol: '$'
                }
            });
            numeral.language('es');
            var format = numeral(value).format('0,0.00');
            numeral.language(lang);
            return format;
        };

        export var Fixed = (value: number): string => {
            return value > 9 ? value.toString() : "0" + value;
        };

        export var Phone = (value: string): string => {
            var areaCode = value.substring(0, value.indexOf("-"));
            var number = value.substring(value.indexOf("-") + 1, value.length);
            var numberPart2 = number.substring(number.length - 4, number.length);
            var numberPart1 = number.substring(0, number.length - 4);

            return "(" + areaCode + ") " + numberPart1 + " " + numberPart2;
        };

        export var CUIL = (value: string): string => {
            var regexp = /^(\d{2})(\d*)(\d{1})(.*)$/g;
            var regexp80 = /^(80)(.*)$/g;
            var resultwith80 = regexp80.exec(value);
            if (resultwith80 && resultwith80.length == 3)
                value = resultwith80[2];

            var result = regexp.exec(value);
            if (result.length == 5 && result[4] == "")
                return (result[1] + "-" + result[2] + "-" + result[3])
            if (result.length == 5)
                return (result[1] + "-" + result[2] + "-" + result[3] + " - " + result[4])
            else
                return value;

        };
    }

    export module DataTables {

        export module Format {
            class yesno {
                render: (data: any, type: any, row: any) => any;
                constructor() {
                    this.render = function (data: any, type: any, row: any): any {
                        var bData = $.parseJSON(data);
                        if (typeof bData == "boolean") return bData ? 'Sí' : 'No';
                        else return data;
                    };
                }
            }

            class money {
                render: (data: any, type: any, row: any) => any;
                constructor() {
                    this.render = function (data: any, type: any, row: any): any {
                        return Common.Format.Money(data);
                    };
                }
            }

            class dashOnNull {
                render: (data: any, type: any, row: any) => any;
                constructor() {
                    this.render = function (data: any, type: any, row: any): any {
                        if (data == null || data == '' || data != null && data.toString().trim() == '') return '-';
                        else return data;
                    };
                }
            }
            class dashOnZero {
                render: (data: any, type: any, row: any) => any;
                constructor() {
                    this.render = function (data: any, type: any, row: any): any {
                        if (data == 0) return '-';
                        else return data;
                    };
                }
            }
            class float {
                render: (data: any, type: any, row: any) => any;
                constructor() {
                    this.render = function (data: any, type: any, row: any): any {
                        return Common.Format.FloatFixed(data);
                    };
                }
            }
            class floatFixed {
                render: (data: any, type: any, row: any) => any;
                constructor() {
                    this.render = function (data: any, type: any, row: any): any {
                        return Common.Format.FloatFixed(data);
                    };
                }
            }

            class small {
                render: (data: any, type: any, row: any) => any;
                constructor() {
                    this.render = function (data: any, type: any, row: any): any {
                        return '<small>' + data + '</small>';
                    };
                }
            }

            export var YesNo = new yesno();
            export var Money = new money();
            export var Float = new float();
            export var FloatFixed = new floatFixed();
            export var DashOnNull = new dashOnNull();
            export var DashOnZero = new dashOnZero();
            export var Small = new small();
            export var Create = function (targets: Array<number>, format: any): any {
                return {
                    targets: targets,
                    render: format.render
                };
            };
        }

    }

    export module Utilities {
        export var Url = (relativeUrl: string, host?: string) => {
            if (host) {
                return host + '/' + relativeUrl;
            }
            return document.location.protocol + '//' + document.location.host + '/' + relativeUrl;
        };

        export var parseMoneyToDecimal = (money) => {
            if (money != null) {
                if ($.isNumeric(money)) {
                    return money;
                };

                return parseFloat(money.replace(",", "&").replace(".", ",").replace("&", "."));
            } else {
                return null;
            }
        };

        export var SelectRow = (table, index) => {
            //TODO: Agregar DataTable para que lo entienda un objeto JQuery
            var $currentRow = $($(table)["DataTable"]().rows().nodes().to$()[index]);
            /*$currentRow.addClass('selected');
            $currentRow.find('td>i').addClass('fa-check-square');*/
            $currentRow.click();
        };

        export var stringToDecimal = (money: any) => {
            if (Object.prototype.toString.call(money) == '[object String]') {
                money = money.replace(",", ".");
            }

            if ($.isNumeric(money)) {
                return parseFloat(money);
            }

            return null;
        };
    }

    export module Cors {
        export function PerformGetHtml(sourceUrl: string, token: string, onSuccess) {
            $.ajax({
                type: 'GET',
                url: sourceUrl,
                context: this,
                headers: {
                    'Authorization': 'Bearer ' + token
                },
                success: onSuccess
            });
        }

        // Realiza el llamado a un action de una sourceUrl dada. 
        // resourcesUrls es un array con las urls de los recursos necesarios para que funcione el html.
        // token es el token necesario para conectarse a tráves de Auth0
        // onSuccess es la función que se retorna en success luego de obtener el html pedido.
        export function GetHtml(sourceUrl: string, resourcesUrls: Array<string>, token: string, onSuccess) {
            if (resourcesUrls == undefined || !$.isArray(resourcesUrls) || resourcesUrls.length == 0) {
                Cors.PerformGetHtml(sourceUrl, token, onSuccess);
            } else {
                var totalJsReady = 0;
                var totalJsToLoad = 2;
                var callbackIn = () => {
                    totalJsReady++;
                    if (totalJsReady == totalJsToLoad) {
                        Cors.PerformGetHtml(sourceUrl, token, onSuccess);
                    }
                }

                for (var i = 0; i < resourcesUrls.length; i++) {
                    $.ajax({
                        type: 'GET',
                        url: resourcesUrls[i],
                        context: this,
                        dataType: "script",
                        headers: {
                            'Authorization': 'Bearer ' + $("#testCors").val()
                        },
                        success: callbackIn
                    });
                }
            }
        }
    }

    export module CourtProceedingDefinition {
        export enum CourtProceedingDefinitionType {
            Trial = 1,
            Agree = 2,
            Mediation = 3
        }

        export function toString(mode: CourtProceedingDefinitionType) {
            if (mode == CourtProceedingDefinitionType.Trial) {
                return "Juicio"
            }
            else if (mode == CourtProceedingDefinitionType.Mediation) {
                return "Mediación"
            }
            else if (mode == CourtProceedingDefinitionType.Agree) {
                return "Acuerdo/Gestión Extrajudicial"
            }
        }

    }

}