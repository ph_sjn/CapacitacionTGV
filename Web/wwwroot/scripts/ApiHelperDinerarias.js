/// <reference path="../../wwwroot/js/typings/natal/nf.d.ts" />
/// <reference path="../../wwwroot/js/typings/jquery/jquery.d.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var ApiHelper;
(function (ApiHelper) {
    var ApiHelperDinerarias = /** @class */ (function (_super) {
        __extends(ApiHelperDinerarias, _super);
        function ApiHelperDinerarias(controller, cors) {
            if (cors === void 0) { cors = false; }
            var _this = _super.call(this, controller, cors) || this;
            var self = _this;
            if (cors == true) {
                self.domain = $("BODY").data('url-dinerarias');
                $.ajax({
                    type: "GET",
                    url: "/api/DinerariasToken",
                    async: true,
                    contentType: 'application/json; charset=utf-8',
                    success: function (data) {
                        self.token = data;
                    }
                });
            }
            return _this;
        }
        return ApiHelperDinerarias;
    }(ApiHelper.ApiHelperBase));
    ApiHelper.ApiHelperDinerarias = ApiHelperDinerarias;
})(ApiHelper || (ApiHelper = {}));
