﻿
Vue.filter('currency', function (value) {
    if (value==null) return ''
    value = value.toString()
    return '$' + parseFloat(Math.round(value * 100) / 100).toFixed(2);
})