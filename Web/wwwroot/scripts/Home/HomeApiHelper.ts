/// <reference path="../../wwwroot/js/typings/natal/nf.d.ts" />

'use strict';
module Web.HomeApiHelper {
    export class Home extends ApiHelper.ApiHelperBase {
        constructor() {
            super('/Home');
        }

        GetDatosHome() {
            return this.get(this.controller + '/GetDatosHome', {});
        }
    }
}