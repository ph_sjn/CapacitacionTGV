﻿/// <reference path="../../wwwroot/js/typings/natal/nf.d.ts" />
/// <reference path="../../wwwroot/js/typings/moment/moment.d.ts" />

'use strict';
module Web.HomeController {
    export class HomeController extends Common.Controller<any> {
        ApiHelper: HomeApiHelper.Home;
        data: any;
        methods: any;
        created: () => void;
        el: string;

        constructor(params) {
            super();
            var self = this;
            this.ApiHelper = new HomeApiHelper.Home();
 
            
            this.created = function () {
                this.getDatosHome();
            }  
     

            this.data = {
                ClientesList: [],
                configColumns: [
                    {
                        title: "Nombre",
                        field: "nombre",
                        visible: true
                    },
                    {
                        title: "Apellido",
                        field: "apellido",
                        visible: true
                    }
                ]
            },


            this.methods = {
                getDatosHome: function () {
                    NF.UI.Page.block();
                    self.ApiHelper.GetDatosHome().done((result: any) => {
                        this.ClientesList = result;
                    }).always(function () {
                        NF.UI.Page.unblock();
                    });
                }


             }



        }

    }
}
