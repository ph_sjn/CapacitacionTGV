﻿/// <binding BeforeBuild='default, copysourcets' Clean='default, copysourcets' />
var gulp = require('gulp');
//var del = require('del');

var paths = {
    scripts: ['scripts/**/*.js', 'scripts/**/*.ts', 'scripts/**/*.map',
        'Features/**/*.ts', 'Features/**/*.map'],
    sourcets: ['Features/**/*.ts'],
};

gulp.task('default', function () {
    gulp.src(paths.scripts).pipe(gulp.dest('wwwroot/scripts'))
});

gulp.task('copysourcets', function () {
    gulp.src(paths.sourcets).pipe(gulp.dest('wwwroot/Features'))
});