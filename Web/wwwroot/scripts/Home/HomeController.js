/// <reference path="../../wwwroot/js/typings/natal/nf.d.ts" />
/// <reference path="../../wwwroot/js/typings/moment/moment.d.ts" />
'use strict';
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Web;
(function (Web) {
    var HomeController;
    (function (HomeController_1) {
        var HomeController = /** @class */ (function (_super) {
            __extends(HomeController, _super);
            function HomeController(params) {
                var _this = _super.call(this) || this;
                var self = _this;
                _this.ApiHelper = new Web.HomeApiHelper.Home();
                _this.created = function () {
                    this.getDatosHome();
                };
                _this.data = {
                    ClientesList: [],
                    configColumns: [
                        {
                            title: "Nombre",
                            field: "nombre",
                            visible: true
                        },
                        {
                            title: "Apellido",
                            field: "apellido",
                            visible: true
                        }
                    ]
                },
                    _this.methods = {
                        getDatosHome: function () {
                            var _this = this;
                            NF.UI.Page.block();
                            self.ApiHelper.GetDatosHome().done(function (result) {
                                _this.ClientesList = result;
                            }).always(function () {
                                NF.UI.Page.unblock();
                            });
                        }
                    };
                return _this;
            }
            return HomeController;
        }(Common.Controller));
        HomeController_1.HomeController = HomeController;
    })(HomeController = Web.HomeController || (Web.HomeController = {}));
})(Web || (Web = {}));
