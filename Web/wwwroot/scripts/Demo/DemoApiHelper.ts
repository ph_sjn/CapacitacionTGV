/// <reference path="../../wwwroot/js/typings/natal/nf.d.ts" />

'use strict';
module Comercial.DemoApiHelper {

    export class Demo extends ApiHelper.ApiHelperBase {
        constructor() {
            super('/Demo');
        }
        
        Search(params: any) {
            return this.get(this.controller + '/Search', params);
        }
    }

}