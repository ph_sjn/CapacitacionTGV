/// <reference path="../wwwroot/js/typings/natal/nf.d.ts" />
/// <reference path="../wwwroot/js/typings/jquery/jquery.d.ts" />
var ApiHelper;
(function (ApiHelper) {
    var ApiHelperBase = /** @class */ (function () {
        function ApiHelperBase(controller, cors) {
            if (cors === void 0) { cors = false; }
            var _this = this;
            this._apiRoot = '/api';
            this.token = '';
            this.domain = '';
            // this.self = this;
            this.controller = this._apiRoot + controller;
            this._showGenericError = false;
            this.get = function (url, params, enableGenericError) {
                _this._showGenericError = enableGenericError != undefined ? enableGenericError : true;
                if (params != undefined) {
                    url += '?' + $.param(params);
                }
                if (cors) {
                    return $.ajax({
                        type: "GET",
                        url: _this.domain + url,
                        contentType: 'application/json; charset=utf-8',
                        error: _this.error,
                        headers: {
                            'Authorization': 'Bearer ' + _this.token
                        },
                    });
                }
                return $.ajax({
                    type: "GET",
                    url: url,
                    contentType: 'application/json; charset=utf-8',
                    error: _this.error
                });
            };
            this.post = function (url, params, data, enableGenericError) {
                _this._showGenericError = enableGenericError != undefined ? enableGenericError : true;
                return _this.makeRequest(url, params, data, "POST");
            };
            this.put = function (url, params, data, enableGenericError) {
                _this._showGenericError = enableGenericError != undefined ? enableGenericError : true;
                return _this.makeRequest(url, params, data, "PUT");
            };
            this.makeRequest = function (url, params, data, verb) {
                if (params != undefined) {
                    url += '?' + $.param(params);
                }
                if (cors) {
                    return $.ajax({
                        type: verb,
                        url: _this.domain + url,
                        data: JSON.stringify(data),
                        contentType: 'application/json; charset=utf-8',
                        error: _this.error,
                        headers: {
                            'Authorization': 'Bearer ' + _this.token
                        },
                    });
                }
                return $.ajax({
                    type: verb,
                    url: url,
                    data: JSON.stringify(data),
                    contentType: 'application/json; charset=utf-8',
                    error: _this.error
                });
            };
            this.delete = function (url, params, data) {
                return _this.makeRequest(url, params, data, 'DELETE');
            };
            this.hasErrors = function () {
                return !NF.Validation.validate();
            };
            this.error = function (jqXHR, textStatus, errorThrow) {
                if (!_this._showGenericError) {
                    return;
                }
                // Natal.Validation.showErrors(null, jqXHR.responseJSON.ModelState);
                var message = '';
                switch (jqXHR.status) {
                    case 400:
                        message = jqXHR.responseJSON != undefined ? jqXHR.responseJSON.Message : jqXHR.responseText;
                        break;
                    case 403:
                        ///Para que no se muestre la notificacion de la excepcion.
                        return;
                    case 404:
                        return;
                    default:
                        message = 'Se ha producido un error en el sistema, comuniquese con el Administrador (' + jqXHR.status + ')';
                        break;
                }
                NF.Notification.show({
                    type: NF.Notification.Type.ERROR,
                    content: message,
                    clickClose: true
                });
            };
        }
        return ApiHelperBase;
    }());
    ApiHelper.ApiHelperBase = ApiHelperBase;
})(ApiHelper || (ApiHelper = {}));
