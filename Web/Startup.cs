﻿
using Contract;
using Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Services;
using Web.Infrastructure;

namespace Web
{
    public class Startup
    {
        private readonly IHostingEnvironment _currentEnv;

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;

            _currentEnv = env;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Modificando las Rutas
            services.AddMvc().AddFeatureFolders();

            //Base da Datos
            services.ConfigureDatabase(Configuration);

            //Injectando Contract y Service
            services.AddScoped<IClienteService, ClienteService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //Ejemplo Middleware
            /*
            app.Run(async context =>
            {
                //if (context.Request.QueryString.HasValue) {

                    await context.Response.WriteAsync("Hello, World!");
                //}
            });*/

            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
