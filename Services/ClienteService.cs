﻿using Contract;
using Data;
using Data.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public class ClienteService : IClienteService
    {
        private readonly NetCoreDatabaseContext _context;

        public ClienteService(NetCoreDatabaseContext context)
        {
            _context = context;
        }

        public IEnumerable<Clientes> Search()
        {
            var result = _context.Clientes.Select(x => new Clientes
            {
                Id = x.Id,
                Nombre = x.Nombre,
                Apellido = x.Apellido
            }).ToList();

            return result;
        }
    }
}
