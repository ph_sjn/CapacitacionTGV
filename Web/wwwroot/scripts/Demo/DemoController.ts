/// <reference path="../../wwwroot/js/typings/natal/nf.d.ts" />

'use strict';
module Comercial.DemoController {
    export class DemoController extends Common.Controller<any> {

        ApiHelper: DemoApiHelper.Demo;
        data: any;
        el: string;
        methods: any;
        search: void;

        constructor(params) {
            super();
            var self = this;
            this.ApiHelper = new DemoApiHelper.Demo();
            this.el = "#Demo";
            this.data = {
                texto: "Demo Yama",
                check:  true,
                switch: true,
                fecha: null,
                fechayear: null,
                results: [],
                autocomplete: null,
                filterBy: null,
                srcautocompleteremoto: null,
                autocompleteremoto: null,
                resultsAutoComplete: [
                    { id: 1, text: 'Oficina' },
                    { id: 2, text: 'Productor' },
                    { id: 3, text: 'Representante' }],
                
                configColumns: [
                    {
                        title: "Nro. Siniestro",
                        field: "LossNumber",
                        visible: true,
                        editor: true
                    },
                    {
                        title: "Fecha Accidente",
                        field: "AccidentDate",
                        visible: true,
                        editor: {
                            type: 'mask',
                            mask: 'date'
                        },
                        format: 'DD/MM/YYYY'
                    
                    }
                ]
            };
          
            this.methods = {
                search: function () {
                    console.log("Buscando...");
                      
                    self.ApiHelper.Search({}).done((result: any) => {
                        console.log(result);
                        self.data.results = result;
                    })
                }               
            }
        }
    }
    

}