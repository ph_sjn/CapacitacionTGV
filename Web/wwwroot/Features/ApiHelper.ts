/// <reference path="../wwwroot/js/typings/natal/nf.d.ts" />
/// <reference path="../wwwroot/js/typings/jquery/jquery.d.ts" />

module ApiHelper {
    export class ApiHelperBase {
        private _apiRoot: string = '/api';
        token: string = '';
        domain: string = '';
        private _showGenericError: boolean;
        controller: string;
        // self: any;
        get: (url: string, params: any, enableGenericError?: boolean) => any;
        post: (url: string, params: any, data: any, enableGenericError?: boolean) => any;
        put: (url: string, params: any, data: any, enableGenericError?: boolean) => any;
        private makeRequest: (url: string, params: any, data: any, verb: string) => any;
        delete: (url: string, params, data) => any;
        hasErrors: () => boolean;
        error: (jqXHR: JQueryXHR, textStatus: string, errorThrow: string) => any;


        constructor(controller: string, cors: boolean = false) {
            // this.self = this;
            this.controller = this._apiRoot + controller;
            this._showGenericError = false;

            this.get = (url: string, params: any, enableGenericError?: boolean) => {

                this._showGenericError = enableGenericError != undefined ? enableGenericError : true;
                if (params != undefined) {
                    url += '?' + $.param(params);
                }
                if (cors) {
                    return $.ajax({
                        type: "GET",
                        url: this.domain + url,
                        contentType: 'application/json; charset=utf-8',
                        error: this.error,
                        headers: {
                            'Authorization': 'Bearer ' + this.token
                        },
                    });
                }
                return $.ajax({
                    type: "GET",
                    url: url,
                    contentType: 'application/json; charset=utf-8',
                    error: this.error
                });
            };

            this.post = (url: string, params: any, data: any, enableGenericError?: boolean) => {
                this._showGenericError = enableGenericError != undefined ? enableGenericError : true;
                return this.makeRequest(url, params, data, "POST");
            };

            this.put = (url: string, params: any, data: any, enableGenericError?: boolean) => {
                this._showGenericError = enableGenericError != undefined ? enableGenericError : true;
                return this.makeRequest(url, params, data, "PUT");
            };

            this.makeRequest = (url: string, params: any, data: any, verb: string) => {
                if (params != undefined) {
                    url += '?' + $.param(params);
                }

                if (cors) {
                    return $.ajax({
                        type: verb,
                        url: this.domain + url,
                        data: JSON.stringify(data),
                        contentType: 'application/json; charset=utf-8',
                        error: this.error,
                        headers: {
                            'Authorization': 'Bearer ' + this.token
                        },
                    });
                }

                return $.ajax({
                    type: verb,
                    url: url,
                    data: JSON.stringify(data),
                    contentType: 'application/json; charset=utf-8',
                    error: this.error
                });
            };

            this.delete = (url: string, params, data) => {
                return this.makeRequest(url, params, data, 'DELETE');
            };

            this.hasErrors = () => {
                return !NF.Validation.validate();
            };

            this.error = (jqXHR: JQueryXHR, textStatus: string, errorThrow: string) => {
                if (!this._showGenericError) {
                    return;
                }

                // Natal.Validation.showErrors(null, jqXHR.responseJSON.ModelState);

                var message = '';
                switch (jqXHR.status) {
                    case 400:
                        message = jqXHR.responseJSON != undefined ? jqXHR.responseJSON.Message : jqXHR.responseText;
                        break;
                    case 403:
                        ///Para que no se muestre la notificacion de la excepcion.
                        return;
                    case 404:
                        return;
                    default:
                        message = 'Se ha producido un error en el sistema, comuniquese con el Administrador (' + jqXHR.status + ')';
                        break;
                }
                NF.Notification.show({
                    type: NF.Notification.Type.ERROR,
                    content: message,
                    clickClose: true
                });


            };
        }
    }
}